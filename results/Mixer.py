#!/usr/bin/env python
from glob import glob
import librosa
import argparse
import numpy as np
import random
import soundfile as sf
import os
from tqdm import tqdm

class Mixer:
    def __init__(self, clean_speech_dir, noise_dir, out_dir, snr):
        self.clean_speech = glob(clean_speech_dir + '*.wav')
        self.noise = glob(noise_dir + '*.wav')
        # random.shuffle(self.noise)
        self.noisy_speech_dir = out_dir + 'noisy' + str(snr) + '/'
        if not os.path.exists(self.noisy_speech_dir):
            os.makedirs(self.noisy_speech_dir)
        self.snr = snr

    def mix(self):
        for speech_file in tqdm(self.clean_speech):
            clean_wav, sr = librosa.load(speech_file, sr=None)
            noise_wav, srn = librosa.load(random.choice(self.noise), sr=None)
            mixed_wav = self.mix_audio(clean_wav, noise_wav, self.snr)
            sf.write(self.noisy_speech_dir + 'noisy_' + speech_file.split("/")[-1], mixed_wav, sr)

    def mix_audio(self, signal, noise, snr):
        # if the audio is longer than the noise
        # play the noise in repeat for the duration of the audio
        noise = noise[np.arange(len(signal)) % len(noise)]

        # if the audio is shorter than the noi
        # this is important if loading resulted in
        # uint8 or uint16 types, because it would cause overflow
        # when squaring and calculating mean
        noise = noise.astype(np.float32)
        signal = signal.astype(np.float32)

        # get the initial energy for reference
        signal_energy = np.mean(signal ** 2)
        noise_energy = np.mean(noise ** 2)
        # calculates the gain to be applied to the noise
        # to achieve the given SNR
        g = np.sqrt(10.0 ** (-snr / 10) * signal_energy / noise_energy)

        # mix the signals
        return signal + g * noise

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='Mixer')
    parser.add_argument("clean_speech_dir")
    parser.add_argument("noise_dir")
    parser.add_argument("out_dir")
    parser.add_argument("snr", type=int)
    args = parser.parse_args()

    mixer = Mixer(clean_speech_dir=args.clean_speech_dir,
                  noise_dir=args.noise_dir,
                  out_dir=args.out_dir,
                  snr=args.snr)

    mixer.mix()
