import pickle
import numpy as np

models = ['AugNet', 'DeepFilterNet2', 'DeepFilterNet2_256', 'DeepFilterNet2_152', 'DeepFilterNet2_128']
restypes = ['STOI', 'PESQ', 'SISDR', 'RTF']
noises = ['40', '20', '10', '5', '0', '-5']
for noise in noises:
    print("="*50)
    print("Noise:", noise)
    for restype in restypes:
        print('\tMeasure:', restype)
        for model in models:
            try:
                with open(f'results/{model}/{model}_noisy{noise}_{restype}', 'rb') as fp:
                    res = pickle.load(fp)
                    print(f'\t\t{model}: {np.mean(res):.3f}')
            except:
                pass
        print('')