#!/usr/bin/env python
import glob
import logging
import os
import subprocess
import numpy as np
import argparse
from multiprocessing import Pool, cpu_count
from pathlib import Path
from typing import List
from tqdm import tqdm
import yaml
import librosa
import torch
from torchmetrics.functional.audio import stoi
from torchmetrics.audio.pesq import PerceptualEvaluationSpeechQuality
from torchmetrics.functional import scale_invariant_signal_distortion_ratio
import pickle
from contextlib import redirect_stdout
from io import StringIO
import sys
import soundfile as sf


class Config:
    def __init__(self,
                 model_name,
                 model_path,
                 lookahead_delay_ms,
                 predict_file_list_path,
                 clean_predict_file_list_path,
                 predict_out_file_list_path,
                 snr):
        self.model_name = model_name
        self.model_path = model_path
        self.lookahead_delay_ms = lookahead_delay_ms
        self.predict_file_list_path = predict_file_list_path
        self.clean_predict_file_list_path = clean_predict_file_list_path
        self.predict_out_file_list_path = predict_out_file_list_path
        self.snr = snr


class Tester:
    """Activities done after training for model testing purposes"""

    def __init__(self, cfg: Config, step: int):
        self.cfg = cfg
        self.noisy_files: List[str] = []
        self.predicted_files: List[str] = []
        self.clean_files: List[str] = []
        self.step = step
        self.crop_duration = cfg.lookahead_delay_ms / 1000
        self.prediction_time = []
        self.stoi = []
        self.pesq = []
        self.sisdr = []
        self.rtf = []

    def __call__(self) -> None:
        #     """Execute testing pipeline"""
        #     # self.build_model()
        self.get_files()
        if len(self.noisy_files) != len(self.clean_files):
            print(self.cfg.predict_file_list_path)
            print(f'Not the same number of noisy and clean files:')
            print(f'\tNumber of noisy files: {len(self.noisy_files)}')
            print(f'\tNumber of clean files: {len(self.clean_files)}')
            return
        if len(self.noisy_files) != len(self.predicted_files):
            print(f'Not all files have been predicted. Predicting from index: {len(self.predicted_files)}')
            self.predict_files(checkpoint=len(self.predicted_files))
            self.get_files()
        if self.cfg.model_name == 'AugNet' or self.cfg.model_name == 'RNNoise':
            self.calculate_rtf()
        else:
            try:
                with open(f'results/{self.cfg.model_name}_{self.cfg.predict_file_list_path.split("/")[-2]}_RTF',
                          'rb') as fp:
                    self.rtf = pickle.load(fp)
            except:
                print(f'No rtf file was found for this model.')
        self.calculate_stoi_pesq_sisdr()
        self.print_results()

    def get_files(self) -> None:
        """Find testing files and store them for later"""

        if self.cfg.model_name == 'RNNoise':
            self.noisy_files = glob.glob(os.path.join(self.cfg.predict_file_list_path, '*.raw'))
            self.clean_files = glob.glob(os.path.join(self.cfg.clean_predict_file_list_path, "*.raw"))
            self.predicted_files = glob.glob(os.path.join(self.cfg.predict_out_file_list_path, "*.raw"))
        else:
            self.noisy_files = glob.glob(os.path.join(self.cfg.predict_file_list_path, '*.wav'))
            self.clean_files = glob.glob(os.path.join(self.cfg.clean_predict_file_list_path, "*.wav"))
            self.predicted_files = glob.glob(os.path.join(self.cfg.predict_out_file_list_path, "*.wav"))

    def predict_files_multi(self, file, new_file_path):
        out = subprocess.run([self.cfg.model_path, file, new_file_path], stdout=subprocess.PIPE)
        return out.stdout.decode('utf-8')

    def predict_files_deepFilt(self, path, model_path, out_path):
        subprocess.run(["python3", "DeepFilterNet/DeepFilterNet/df/enhance_v2.py", "-m", model_path, "-o", out_path, "-D", "-s", self.cfg.snr, path])

    def predict_files_multi_rnnoise(self, file, new_file_path):
        out = subprocess.run([self.cfg.model_path, file, new_file_path], stdout=subprocess.PIPE)
        return out.stdout.decode('utf-8')

    # def raw2wav(self, file, new_file_path):
    #     subprocess.run(['sox -t raw -r 44100 -b 16 -c 1 -L -e signed-integer', file, new_file_path])

    def predict_files(self, checkpoint) -> None:
        """Denoise testing files using built AugNet"""
        print('Processing test_noise samples')
        new_file_path_list = []
        for file in self.noisy_files[checkpoint:]:
            dir_name, file_name = os.path.split(file)
            new_file_path = os.path.join(self.cfg.predict_out_file_list_path, file_name)
            new_file_path_list.append(new_file_path)
        inputs = zip(self.noisy_files[checkpoint:], new_file_path_list)

        if self.cfg.model_name == "AugNet":
            with Pool(cpu_count()) as pool:
                self.prediction_time = pool.starmap(self.predict_files_multi, tqdm(inputs, total=len(new_file_path_list)))
            self.prediction_time = [float(t.split(' ')[2]) for t in self.prediction_time]
        elif self.cfg.model_name == 'RNNoise':
            with Pool(cpu_count()) as pool:
                self.prediction_time = pool.starmap(self.predict_files_multi_rnnoise, tqdm(inputs, total=len(new_file_path_list)))
            self.prediction_time = list(map(float, self.prediction_time))
        else:
            self.predict_files_deepFilt(self.cfg.predict_file_list_path, self.cfg.model_path, self.cfg.predict_out_file_list_path)

        self.predicted_files = new_file_path_list

    def calculate_stoi_pesq_sisdr(self):
        """Calculate STOI metrics for testing files."""
        print('Calculating STOI, PESQ and SI-SDR')

        try:
            with open(f'results/{self.cfg.model_name}_{self.cfg.predict_file_list_path.split("/")[-2]}_STOI', 'rb') as fp:
                self.stoi = pickle.load(fp)
            with open(f'results/{self.cfg.model_name}_{self.cfg.predict_file_list_path.split("/")[-2]}_PESQ', 'rb') as fp:
                self.pesq = pickle.load(fp)
            with open(f'results/{self.cfg.model_name}_{self.cfg.predict_file_list_path.split("/")[-2]}_SISDR', 'rb') as fp:
                self.sisdr = pickle.load(fp)
        except:
            self.stoi = []
            self.pesq = []
            self.sisdr = []
        num_calculated = len(self.stoi)
        if num_calculated == len(self.clean_files):
            print('\tAlready calculated')
            return
        for i, (clean_file, predicted_file) in \
                tqdm(enumerate(zip(self.clean_files[num_calculated:], self.predicted_files[num_calculated:])),
                     total=len(self.clean_files[num_calculated:])):
            clean_wav, sr = librosa.load(clean_file, sr=None)
            crop_samples = int(sr * self.crop_duration)
            if self.crop_duration > 0:
                clean_wav = clean_wav[:-crop_samples]
            clean_wav = torch.tensor(clean_wav)
            if self.cfg.model_name == 'RNNoise':
                predicted_wav, sr = sf.read(predicted_file, channels=1, samplerate=48000, format='RAW', subtype="PCM_16")
            else:
                predicted_wav, sr = librosa.load(predicted_file, sr=None)
            if self.crop_duration > 0:
                predicted_wav = predicted_wav[crop_samples:]
            predicted_wav = torch.tensor(predicted_wav)

            # STOI
            result_stoi = stoi.short_time_objective_intelligibility(clean_wav, predicted_wav, fs=sr)
            self.stoi.append(result_stoi.item())

            # PESQ
            clean_wav_16k = librosa.resample(np.asarray(clean_wav), orig_sr=sr, target_sr=16000)
            clean_wav_16k = torch.tensor(clean_wav_16k)
            predicted_wav_16k = librosa.resample(np.asarray(predicted_wav), orig_sr=sr, target_sr=16000)
            predicted_wav_16k = torch.tensor(predicted_wav_16k)
            wb_pesq = PerceptualEvaluationSpeechQuality(16000, 'wb')
            result_pesq = wb_pesq(predicted_wav_16k, clean_wav_16k)
            self.pesq.append(result_pesq.item())

            # SI-SDR
            self.sisdr.append(scale_invariant_signal_distortion_ratio(clean_wav, predicted_wav).item())

            # Checkpoint write
            if not i % 100:
                with open(f'results/{self.cfg.model_name}_{self.cfg.predict_file_list_path.split("/")[-2]}_STOI', 'wb') as fp:
                    pickle.dump(self.stoi, fp)
                with open(f'results/{self.cfg.model_name}_{self.cfg.predict_file_list_path.split("/")[-2]}_PESQ', 'wb') as fp:
                    pickle.dump(self.pesq, fp)
                with open(f'results/{self.cfg.model_name}_{self.cfg.predict_file_list_path.split("/")[-2]}_SISDR', 'wb') as fp:
                    pickle.dump(self.sisdr, fp)

        # Final write
        with open(f'results/{self.cfg.model_name}_{self.cfg.predict_file_list_path.split("/")[-2]}_STOI', 'wb') as fp:
            pickle.dump(self.stoi, fp)
        with open(f'results/{self.cfg.model_name}_{self.cfg.predict_file_list_path.split("/")[-2]}_PESQ', 'wb') as fp:
            pickle.dump(self.pesq, fp)
        with open(f'results/{self.cfg.model_name}_{self.cfg.predict_file_list_path.split("/")[-2]}_SISDR', 'wb') as fp:
            pickle.dump(self.sisdr, fp)

    def calculate_rtf(self):
        print('Calculating Real Time Factor')
        try:
            with open(f'results/{self.cfg.model_name}_{self.cfg.predict_file_list_path.split("/")[-2]}_RTF', 'rb') as fp:
                self.rtf = pickle.load(fp)
        except:
            self.rtf = []

        num_calculated = len(self.rtf)
        if num_calculated == len(self.clean_files):
            print('\tAlready calculated')
            return
        print()
        for i, (clean_file, predicted_time) in \
                tqdm(enumerate(zip(self.clean_files[num_calculated:], self.prediction_time[num_calculated:])),
                     total=len(self.prediction_time[num_calculated:])):
            if self.cfg.model_name == 'RNNoise':
                clean_wav, sr = sf.read(clean_file, channels=1, samplerate=48000, format='RAW',
                                            subtype="PCM_16")
            else:
                clean_wav, sr = librosa.load(clean_file, sr=None)
            t_audio = clean_wav.shape[-1] / sr
            self.rtf.append(predicted_time / t_audio)

            # Checkpoint write
            if not i % 100:
                with open(f'results/{self.cfg.model_name}_{self.cfg.predict_file_list_path.split("/")[-2]}_RTF', 'wb') as fp:
                    pickle.dump(self.rtf, fp)

        # Final write
        with open(f'results/{self.cfg.model_name}_{self.cfg.predict_file_list_path.split("/")[-2]}_RTF', 'wb') as fp:
            pickle.dump(self.rtf, fp)
        print('Final RFT:', np.mean(self.rtf))

    def print_results(self):
        print(f'\n{"-" * 80}')
        print(f'Model: \t{str(self.cfg.model_name)}')
        print(f'Data: \t{str(self.cfg.predict_file_list_path)}\n')
        if self.stoi:
            print(f'STOI: {np.mean(self.stoi)}')
        if self.pesq:
            print(f'PESQ: {np.mean(self.pesq)}')
        if self.sisdr:
            print(f'SI-SDR: {np.mean(self.sisdr)}')
        if self.rtf:
            print(f'RTF:  {np.mean(self.rtf)}')
        print('-' * 80)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='Tester')
    parser.add_argument("model_name")
    parser.add_argument("model_path")
    parser.add_argument("lookahead_delay_ms", type=int)
    parser.add_argument("predict_file_list_path")
    parser.add_argument("clean_predict_file_list_path")
    parser.add_argument("predict_out_file_list_path")
    parser.add_argument("snr")

    args = parser.parse_args()

    # model_name = "RNNoise"
    # model_path = "rnnoise/examples/rnnoise_demo"
    # lookahead_delay_ms = 0
    # predict_file_list_path = "minitest/test/n_noisy"
    # clean_predict_file_list_path = "minitest/n_speech_wav/"
    # snr = "40"
    # predict_out_file_list_path = "rnnoise/enhance/enhance"
    # args = Config(model_name=model_name,
    #                 model_path=model_path,
    #                 lookahead_delay_ms=lookahead_delay_ms,
    #                 predict_file_list_path=predict_file_list_path,
    #                 clean_predict_file_list_path=clean_predict_file_list_path,
    #                 predict_out_file_list_path=predict_out_file_list_path,
    #                 snr=snr)

    # model_name = "DeepFilterNet2"
    # model_path = "DeepFilterNet/DeepFilterNet/DeepFilterNet2"
    # lookahead_delay_ms = 0
    # predict_file_list_path = "minitest/test/noisy"
    # clean_predict_file_list_path = "minitest/speech_wav/"
    # snr = "40"
    # predict_out_file_list_path = "DeepFilterNet/DeepFilterNet/DeepFilterNet2/enhance"
    # config1 = Config(model_name=model_name,
    #                 model_path=model_path,
    #                 lookahead_delay_ms=lookahead_delay_ms,
    #                 predict_file_list_path=predict_file_list_path,
    #                 clean_predict_file_list_path=clean_predict_file_list_path,
    #                 predict_out_file_list_path=predict_out_file_list_path,
    #                 snr=snr)

    # model_name = "AugNet"
    # model_path = "AugNet/denoise"
    # lookahead_delay_ms = 40
    # predict_file_list_path = "noisy40_small/"
    # clean_predict_file_list_path = "../data/test_noise/speech_wav_small/"
    # predict_out_file_list_path = "AugNet/enhance40_small/"
    # config = Config(model_name=model_name,
    #                 model_path=model_path,
    #                 lookahead_delay_ms=lookahead_delay_ms,
    #                 predict_file_list_path=predict_file_list_path,
    #                 clean_predict_file_list_path=clean_predict_file_list_path,
    #                 predict_out_file_list_path=predict_out_file_list_path)

    if ',' in args.snr:
        for signal_noise_ratio in args.snr.split(','):
            config = Config(model_name=args.model_name,
                            model_path=args.model_path,
                            lookahead_delay_ms=args.lookahead_delay_ms,
                            predict_file_list_path=args.predict_file_list_path + signal_noise_ratio + '/',
                            clean_predict_file_list_path=args.clean_predict_file_list_path + '/',
                            predict_out_file_list_path=args.predict_out_file_list_path + signal_noise_ratio + '/',
                            snr=signal_noise_ratio)

            if not os.path.exists(config.predict_out_file_list_path):
                os.makedirs(config.predict_out_file_list_path)
            tester = Tester(config, 1)
            tester()
    else:
        config = Config(model_name=args.model_name,
                        model_path=args.model_path,
                        lookahead_delay_ms=args.lookahead_delay_ms,
                        predict_file_list_path=args.predict_file_list_path+args.snr+'/',
                        clean_predict_file_list_path=args.clean_predict_file_list_path+'/',
                        predict_out_file_list_path=args.predict_out_file_list_path+args.snr+'/',
                        snr=args.snr)
        if not os.path.exists(config.predict_out_file_list_path):
            os.makedirs(config.predict_out_file_list_path)

        tester = Tester(config, 1)
        tester()
