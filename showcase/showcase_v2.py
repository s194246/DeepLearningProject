#!/usr/bin/env python3
import tkinter as tk
from tkinter import filedialog as fd
import sounddevice as sd
from scipy.io.wavfile import write
import librosa
import subprocess
from playsound import playsound
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from glob import glob
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import os
import time

# Global constants
SECONDS = 5
SAMPLING_RATE = 48000
BGCOLOR = 'grey'
SCOLOR = 'gray30'
ACOLOR = 'gray70'
BCOLOR = 'gray45'

# variables
recording_file = '__recording'
noisy_file = '__noisy'
enhanced_file = '__enhanced'
recording, noisy_recording = np.array([]), np.array([])
enhanced_recordings = [np.array([]), np.array([])]
all_models = ['DeepFilterNet2', 'DeepFilterNet2_128', 'DeepFilterNet2_152', 'DeepFilterNet2_256']

# counters
rc, nc = 0, 0

model_ec = {model: 0 for model in all_models}

class HoverButton(tk.Button):
    def __init__(self, master, **kw):
        tk.Button.__init__(self, master=master, **kw)
        self.defaultBackground = self["background"]
        self.bind("<Enter>", self.on_enter)
        self.bind("<Leave>", self.on_leave)

    def on_enter(self, e):
        if self['state'] != 'disabled':
            self['background'] = self['activebackground']

    def on_leave(self, e):
        if self['state'] != 'disabled':
            self['background'] = self.defaultBackground

class HoverRadioButton(tk.Radiobutton):
    def __init__(self, master, **kw):
        tk.Radiobutton.__init__(self, master=master, **kw)
        self.defaultBackground = self["background"]
        self.bind("<Enter>", self.on_enter)
        self.bind("<Leave>", self.on_leave)

    def on_enter(self, e):
        if self['state'] != 'disabled':
            self['background'] = self['activebackground']

    def on_leave(self, e):
        if self['state'] != 'disabled':
            self['background'] = self.defaultBackground


root = tk.Tk()
root.geometry('1290x700')
root.resizable(False, False)
root.title('DerpFilterNet Demo')
root.configure(background=BGCOLOR)

f1 = tk.Frame(root, width=1290, height=150)
b1 = tk.Frame(root, width=1290, height=10)
b2 = tk.Frame(root, width=10, height=620)
f1.configure(background=BGCOLOR)
for b in [b1, b2]:
    b.configure(background=SCOLOR)
c1 = tk.Frame(root, width=500, height=200)
c2 = tk.Frame(root, width=500, height=200)
c3 = tk.Frame(root, width=500, height=200)
c4 = tk.Frame(root, width=500, height=200)
cs = [c1, c2, c3, c4]
for c in cs:
    c.configure(background=BGCOLOR)
cb1 = tk.Frame(root, width=140, height=200)
cb2 = tk.Frame(root, width=140, height=200)
cb3 = tk.Frame(root, width=140, height=200)
cb4 = tk.Frame(root, width=140, height=200)
cbs = [cb1, cb2, cb3, cb4]
for cb in cbs:
    cb.configure(background=BGCOLOR)
ct1 = tk.Frame(root, width=500, height=80)
ct2 = tk.Frame(root, width=500, height=80)
ct3 = tk.Frame(root, width=500, height=80)
ct4 = tk.Frame(root, width=500, height=80)
cts = [ct1, ct2, ct3, ct4]
for ct in cts:
    ct.configure(background=BGCOLOR)

canvas = [None]*4
vmin = None

f1.grid(row=0, column=0, columnspan=5)
#f2.grid(row=0, column=4)
b1.grid(row=1, column=0, columnspan=5)
b2.grid(row=2, column=2, rowspan=4)
c1.grid(row=3, column=0)
c2.grid(row=5, column=0)
c3.grid(row=3, column=4)
c4.grid(row=5, column=4)
cb1.grid(row=3, column=1)
cb2.grid(row=5, column=1)
cb3.grid(row=3, column=3)
cb4.grid(row=5, column=3)
ct1.grid(row=2, column=0, columnspan=2)
ct2.grid(row=4, column=0, columnspan=2)
ct3.grid(row=2, column=3, columnspan=2)
ct4.grid(row=4, column=3, columnspan=2)

play_buttons = [None]*4

def load_sound():
    global rc, recording
    filename = fd.askopenfilename(
        title='Select a file',
        initialdir=os.path.join(os.getcwd(), 'template_sounds'),
        filetypes=(('WAV files', '*.wav'),))
    if filename == '':
        return

    button2['state'] = 'normal'
    button3['state'] = 'normal'

    recording, _ = librosa.load(filename, sr=None)
    write(recording_file + str(rc) + '.wav', SAMPLING_RATE, recording)  # Save as WAV file
    rc += 1
    plot_STFT(recording, 0)

def record_sound():
    global rc, recording
    button2['state'] = 'normal'
    button3['state'] = 'normal'
    countdown = tk.Label(root, text='5', font=('Arial', 60), width=3, height=2, background=ACOLOR)
    countdown.grid(row=0, column=0, rowspan=6, columnspan=5)
    countdown.tkraise()
    root.update()
    try:
        record = sd.rec(int(SECONDS * SAMPLING_RATE), samplerate=SAMPLING_RATE, channels=1)
    except sd.PortAudioError:
        raise "There is no microphone!"
    print('Recording...')
    for tt in range(4, 0, -1):
        time.sleep(0.9)
        countdown.configure(text=str(tt))
        root.update()
    sd.wait()  # Wait until recording is finished
    countdown.destroy()
    write(recording_file+str(rc)+'.wav', SAMPLING_RATE, record)  # Save as WAV file
    recording, _ = librosa.load(recording_file+str(rc)+'.wav', sr=None)
    rc += 1
    plot_STFT(recording, 0)
    print('Done!')
    return recording

def add_noise():
    global recording, nc, noisy_recording
    button3['state'] = 'normal'
    noise, _ = librosa.load('showcase_noise/' + noise_name.get().lower() + '.wav', sr=None)
    noise = noise[np.arange(len(recording)) % len(noise)]
    noise = noise.astype(np.float32)
    signal = recording.astype(np.float32)
    signal_energy = np.mean(signal ** 2)
    noise_energy = np.mean(noise ** 2)
    g = np.sqrt(10.0 ** (-snr.get() / 10) * signal_energy / noise_energy)
    noisy_signal = recording + g * noise
    write(noisy_file+str(nc)+'.wav', SAMPLING_RATE, noisy_signal)
    noisy_recording, _ = librosa.load(noisy_file+str(nc)+'.wav', sr=None)
    nc += 1
    plot_STFT(noisy_recording, 1)

def enhance():
    global model_ec, enhanced_recordings
    button3['state'] = 'disabled'
    button3['background'] = BCOLOR
    if play_buttons[1] is not None:
        outputfile = noisy_file+str(nc-1)+'.wav'
    else:
        outputfile = recording_file+str(rc-1)+'.wav'
    countdown = tk.Label(root, text='', font=('Arial', 30), width=30, height=2,
                         background=ACOLOR)
    countdown.grid(row=0, column=0, rowspan=6, columnspan=5)
    countdown.tkraise()

    for i_m, model in enumerate([am1, am2]):
        if canvas[i_m+2]:
            continue
        countdown.configure(text=f'Enhancing with {model.get()}')
        root.update()
        subprocess.run(
            ['python',
             '../DeepFilterNet/DeepFilterNet/df/enhance.py',
             '-m',
             '../DeepFilterNet/DeepFilterNet/'+model.get(),
             '-o',
             enhanced_file+str(model_ec[model.get()])+model.get(),
             outputfile,
             '-D']
        )
        enhanced_recordings[i_m], _ = librosa.load(glob(enhanced_file+str(model_ec[model.get()])+model.get()+'/*.wav')[0], sr=None)
        plot_STFT(enhanced_recordings[i_m], i_m+2)
        model_ec[model.get()] += 1
    countdown.destroy()

def play_recording():
    playsound(recording_file+str(rc-1)+'.wav')

def play_noisy_recording():
    playsound(noisy_file+str(nc-1)+'.wav')

def play_enhanced1_recording():
    playsound(glob(enhanced_file+str(model_ec[am1.get()]-1)+am1.get()+'/*.wav')[0])

def play_enhanced2_recording():
    playsound(glob(enhanced_file+str(model_ec[am1.get()]-1)+am2.get()+'/*.wav')[0])

play_funcs = [play_recording, play_noisy_recording, play_enhanced1_recording, play_enhanced2_recording]

def plot_STFT(sig, idx):
    global canvas, vmin
    fig, ax = plt.subplots(1, 1, figsize=(5, 2), dpi=100)
    fig.patch.set_facecolor(BGCOLOR)
    frequencies, times, spectrogram = signal.spectrogram(sig, SAMPLING_RATE, nperseg=256, noverlap=48, nfft=2048)
    nspec = np.log(spectrogram)
    if idx == 0:
        vmin = (2*np.mean(nspec)+np.min(nspec))/3
    ax.set_xlabel('Time [s]', fontsize=8)
    ax.set_ylabel('Frequency [kHz]', fontsize=8)
    ax.tick_params(axis='both', which='major', labelsize=8)
    # ax.text(-0.5, 12, name, ha='center', va='center', rotation=90, fontsize=16)
    ax.pcolormesh(times, frequencies / 1000, nspec, cmap='jet', vmin=vmin)
    plt.tight_layout()
    if idx < 2:
        for i in range(idx, len(canvas)):
            if canvas[i]:
                remove_plot(i)
    else:
        if canvas[idx]:
            remove_plot(idx)

    canvas[idx] = FigureCanvasTkAgg(fig, master=cs[idx])
    canvas[idx].draw()
    canvas[idx].get_tk_widget().pack()  # grid(row=0, column=0)
    play_buttons[idx] = HoverButton(
        cbs[idx],
        text='Play',
        command=play_funcs[idx],
        background=BCOLOR,
        activebackground=ACOLOR,
        width=6,
        height=2
    )
    if idx < 2:
        play_buttons[idx].pack(padx=(10, 78))
    else:
        play_buttons[idx].pack(padx=(78, 10))

def remove_plot(idx):
    global canvas, play_buttons
    canvas[idx].get_tk_widget().destroy()
    canvas[idx] = None
    play_buttons[idx].destroy()
    play_buttons[idx] = None

def remove_model1_plot(*args):
    global canvas, play_buttons, button2
    if canvas[2]:
        canvas[2].get_tk_widget().destroy()
        canvas[2] = None
        play_buttons[2].destroy()
        play_buttons[2] = None
    button3['state'] = 'normal'

def remove_model2_plot(*args):
    global canvas, play_buttons
    if canvas[3]:
        canvas[3].get_tk_widget().destroy()
        canvas[3] = None
        play_buttons[3].destroy()
        play_buttons[3] = None
    button3['state'] = 'normal'

but_padx, but_pady = (20, 20), (20, 20)
bw, bh = 15, 2
button0 = HoverButton(
    f1,
    text='Load sound',
    command=load_sound,
    background=BCOLOR,
    activebackground=ACOLOR,
    width=bw,
    height=bh
)
button0.grid(row=0, column=1, padx=but_padx, pady=but_pady, sticky='ew')
button1 = HoverButton(
    f1,
    text='Record sound',
    command=record_sound,
    background=BCOLOR,
    activebackground=ACOLOR,
    width=bw,
    height=bh
)
button1.grid(row=0, column=0, padx=but_padx, pady=but_pady, sticky='ew')
button2 = HoverButton(
    f1,
    text='Add noise',
    state='disabled',
    command=add_noise,
    background=BCOLOR,
    activebackground=ACOLOR,
    width=bw,
    height=bh
)
button2.grid(row=0, column=3, padx=but_padx, pady=but_pady, sticky='ew')
button3 = HoverButton(
    f1,
    text='Enhance signal',
    state='disabled',
    command=enhance,
    background=BCOLOR,
    activebackground=ACOLOR,
    width=bw,
    height=bh
)
button3.grid(row=0, column=2, padx=but_padx, pady=but_pady, sticky='ew')

# Add titles
am1 = tk.StringVar(value='DeepFilterNet2_256')
am1.trace("w", remove_model1_plot)
am2 = tk.StringVar(value='DeepFilterNet2_152')
am2.trace("w", remove_model2_plot)

font_pad = (15, 0)
tk.Label(ct1, text="Original", font=('Arial', 20), background=BGCOLOR).grid(sticky='s')
tk.Label(ct2, text="Noisy", font=('Arial', 20), background=BGCOLOR).grid(sticky='s')
# tk.Label(ct3, text=am1.get(), font=('Arial', 20), background=BGCOLOR).grid(row=0, column=0, sticky='s', pady=font_pad)
menu1 = tk.OptionMenu(ct3, am1, *all_models)
menu1.configure(background=BCOLOR, activebackground=ACOLOR, highlightthickness=0, font=('Arial', 20))
menu1.grid(sticky='s')
# tk.Label(ct4, text=am2.get(), font=('Arial', 20), background=BGCOLOR).grid(row=0, column=0, sticky='s')
menu2 = tk.OptionMenu(ct4, am2, *all_models)
menu2.configure(background=BCOLOR, activebackground=ACOLOR, highlightthickness=0, font=('Arial', 20))
menu2.grid(sticky='s')



snr = tk.IntVar(value=40)
tk.Label(f1, text='SNR level:', background=BGCOLOR).grid(row=0, column=4)
slider = tk.Scale(
    f1,
    background=BGCOLOR,
    variable=snr,
    from_=40,
    to=-5,
    orient=tk.HORIZONTAL,
    troughcolor=SCOLOR,
    length=200,
    highlightthickness=0,
    activebackground=ACOLOR
)
slider.grid(row=0, column=5, padx=(10, 10))
noise_name = tk.StringVar(value='river')
tk.Label(f1, text='Noise type:', background=BGCOLOR).grid(row=0, column=6)
noises = [file.split('\\')[1].split('.')[0] for file in glob('showcase_noise/*.wav')]
for i, noise in enumerate(noises):
    HoverRadioButton(f1, text=noise, variable=noise_name, value=noise, background=BGCOLOR, activebackground=ACOLOR).grid(row=0, column=i+7)

root.mainloop()

time.sleep(5)
for file in glob('__*.wav'):
    os.remove(file)
for file in glob('__*/__*.wav'):
    os.remove(file)
for folder in list(os.walk(os.getcwd()))[1:]:
    if not folder[2]:
        os.rmdir(folder[0])
