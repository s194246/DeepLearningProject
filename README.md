Repository for exam project in 02456 Deep Learning

Thank you for your interest in our project. The experiments we conducted in this 
project are all run from the terminal, and they are, unfortunately, too 
extensive to be reproduced in a Jupyter notebook. However, we do have a demo 
that we presented at the poster presentation, which gives a good overview of the 
tool's capability and demonstrates some of the key results. 

The demo is located at ``showcase/showcase_v2.py``.

In addition, code for training can be found in ``DeepFilterNet/``.

Lastly, code for testing the models can be found in ``results/``.

However, note that running any files requires setting up many dependencies in 
Linux/WSL, which is not supported out-of-the-box in this repository.